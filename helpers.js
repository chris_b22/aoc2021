
function transpose( matrix ) {
    let [ row ] = matrix
    return row.map( ( e, i ) => matrix.map( row => row[ i ] ) )
}

function logArrInRows( string, array ) {
    if ( typeof string === 'string' && typeof array === 'object' ) {
        console.log( string )
        array.forEach( e => console.log( e ) )
    }
    if ( typeof string === 'object' && typeof array === 'undefined' ) {
        string.forEach( e => console.log( e ) )
    }
}

export { transpose, logArrInRows }